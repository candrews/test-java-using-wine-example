package com.integralblue.example;

import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.startupcheck.OneShotStartupCheckStrategy;
import org.testcontainers.utility.MountableFile;

import static org.assertj.core.api.Assertions.assertThat;

/** Test that the jar is runnable on Windows by running it using java in wine.
 *
 * The build must have already run and produced a jar at {@code build/libs/example.jar} for this test to work.
 */
/* default */ class WindowsTest {
	@Test
	/* default */ void testHelp() throws Exception {
		try (GenericContainer<?> container = new GenericContainer<>("craigandrews/wine-adoptopenjdk")) {
			container
				.withCopyFileToContainer(MountableFile.forHostPath("build/libs/example.jar"), "this.jar")
				.withCommand("java -jar this.jar")
				.withStartupCheckStrategy(
						new OneShotStartupCheckStrategy().withTimeout(Duration.ofSeconds(20))
						).start();
			assertThat(container.getLogs()).describedAs("Testing that help text contains expected content").contains("Usage: java -jar ");
		}
	}
}
