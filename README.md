# test-java-using-wine-example

Example project demonstrating how to test a Java application in Windows without actually using Windows, and instead using Wine running in Docker.

This example demonstrates setting up Gradle, Testcontainers, and GitLab CI.

See:
* Article explaining the rationale behind the example: [Testing a Java application on Windows without Windows](https://candrews.integralblue.com/2021/02/testing-a-java-application-on-windows-without-windows/)
* [craigandrews/wine-adoptopenjdk](https://hub.docker.com/repository/docker/craigandrews/wine-adoptopenjdk) in Docker Hub
